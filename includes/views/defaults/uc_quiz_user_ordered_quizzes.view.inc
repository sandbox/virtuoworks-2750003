<?php
/**
 * @file
 * Default ordered quizzes views.
 */

$view = new view();
$view->name = 'uc_quiz_user_ordered_quizzes';
$view->description = 'Shows user ordered quizzes';
$view->tag = 'Ubercart And Quiz integration';
$view->base_table = 'uc_orders';
$view->human_name = 'User ordered quizzes';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Mon historique de commandes';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'plus';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'view own orders';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Éléments par page';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Tout -';
$handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Décalage';
$handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
$handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
$handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'created' => 'created',
  'order_id' => 'order_id',
  'order_status' => 'order_status',
  'product_count' => 'product_count',
  'order_total' => 'order_total',
  'actions' => 'actions',
);
$handler->display->display_options['style_options']['default'] = 'created';
$handler->display->display_options['style_options']['info'] = array(
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'order_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => ' ',
    'empty_column' => 0,
  ),
  'order_status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'product_count' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'order_total' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'actions' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Comportement en l'absence de résultats: Global : Zone de texte */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'Pas de commande';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = "Vous n'avez pas encore commandé de tests de connaissances.";
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
$handler->display->display_options['empty']['area']['tokenize'] = TRUE;
/* Relation: Ordre : Produits */
$handler->display->display_options['relationships']['products']['id'] = 'products';
$handler->display->display_options['relationships']['products']['table'] = 'uc_orders';
$handler->display->display_options['relationships']['products']['field'] = 'products';
/* Relation: Ordered product : Nœud */
$handler->display->display_options['relationships']['nid']['id'] = 'nid';
$handler->display->display_options['relationships']['nid']['table'] = 'uc_order_products';
$handler->display->display_options['relationships']['nid']['field'] = 'nid';
$handler->display->display_options['relationships']['nid']['relationship'] = 'products';
$handler->display->display_options['relationships']['nid']['label'] = 'Produits commandés';
$handler->display->display_options['relationships']['nid']['required'] = TRUE;
/* Relation: Entity Reference : Entité référencée */
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['id'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['table'] = 'field_data_field_uc_product_quiz';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['field'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['relationship'] = 'nid';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['label'] = 'Quiz associé';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['required'] = TRUE;
/* Champ: Ordre : Date de création */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'uc_orders';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Date';
$handler->display->display_options['fields']['created']['date_format'] = 'uc_store';
/* Champ: Ordre : Identifiant de la commande */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'uc_orders';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Numéro de commande';
$handler->display->display_options['fields']['order_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['order_id']['link_to_order'] = 0;
/* Champ: Ordre : Statut de la commande */
$handler->display->display_options['fields']['order_status']['id'] = 'order_status';
$handler->display->display_options['fields']['order_status']['table'] = 'uc_orders';
$handler->display->display_options['fields']['order_status']['field'] = 'order_status';
$handler->display->display_options['fields']['order_status']['label'] = 'Etat';
/* Champ: Ordre : Total de la commande */
$handler->display->display_options['fields']['order_total']['id'] = 'order_total';
$handler->display->display_options['fields']['order_total']['table'] = 'uc_orders';
$handler->display->display_options['fields']['order_total']['field'] = 'order_total';
$handler->display->display_options['fields']['order_total']['label'] = 'Montant Total';
$handler->display->display_options['fields']['order_total']['precision'] = '0';
/* Champ: Ordre : Actions */
$handler->display->display_options['fields']['actions']['id'] = 'actions';
$handler->display->display_options['fields']['actions']['table'] = 'uc_orders';
$handler->display->display_options['fields']['actions']['field'] = 'actions';
/* Critère de tri: Ordre : Identifiant de la commande */
$handler->display->display_options['sorts']['order_id']['id'] = 'order_id';
$handler->display->display_options['sorts']['order_id']['table'] = 'uc_orders';
$handler->display->display_options['sorts']['order_id']['field'] = 'order_id';
/* Filtre contextuel: Utilisateur : Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'users';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['exception']['value'] = '';
$handler->display->display_options['arguments']['uid']['exception']['title'] = 'Tout';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'user_or_permission';
$handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';
$handler->display->display_options['arguments']['uid']['validate_options']['perm'] = 'view own orders';
/* Critère de filtrage: Ordre : Statut de la commande */
$handler->display->display_options['filters']['order_status']['id'] = 'order_status';
$handler->display->display_options['filters']['order_status']['table'] = 'uc_orders';
$handler->display->display_options['filters']['order_status']['field'] = 'order_status';
$handler->display->display_options['filters']['order_status']['value'] = array(
  '_active' => '_active',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'user_ordered_quizzes_page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Mes tests de connaissances';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'order_id',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'order_id' => 'order_id',
  'attributes' => 'attributes',
  'title' => 'title',
  'order_product_id_1' => 'order_product_id_1',
  'nid' => 'nid',
  'qty' => 'qty',
  'uc_quiz_field_ordered_quiz_custom_link' => 'uc_quiz_field_ordered_quiz_custom_link',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'order_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'attributes' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'order_product_id_1' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'qty' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'uc_quiz_field_ordered_quiz_custom_link' => array(
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['empty'] = FALSE;
/* Comportement en l'absence de résultats: Global : Zone de texte */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'Pas de commande';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = "Vous n'avez pas encore de commande <strong>validée</strong> de tests de connaissances.";
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
$handler->display->display_options['empty']['area']['tokenize'] = TRUE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relation: Ordre : Produits */
$handler->display->display_options['relationships']['products']['id'] = 'products';
$handler->display->display_options['relationships']['products']['table'] = 'uc_orders';
$handler->display->display_options['relationships']['products']['field'] = 'products';
$handler->display->display_options['relationships']['products']['label'] = 'Produits commandés';
$handler->display->display_options['relationships']['products']['required'] = TRUE;
/* Relation: Ordered product : Nœud */
$handler->display->display_options['relationships']['nid']['id'] = 'nid';
$handler->display->display_options['relationships']['nid']['table'] = 'uc_order_products';
$handler->display->display_options['relationships']['nid']['field'] = 'nid';
$handler->display->display_options['relationships']['nid']['relationship'] = 'products';
$handler->display->display_options['relationships']['nid']['label'] = 'Produit';
$handler->display->display_options['relationships']['nid']['required'] = TRUE;
/* Relation: Entity Reference : Entité référencée */
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['id'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['table'] = 'field_data_field_uc_product_quiz';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['field'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['relationship'] = 'nid';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['label'] = 'Quiz associé au produit';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['required'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Champ: Ordre : Identifiant de la commande */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'uc_orders';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Commande n°';
$handler->display->display_options['fields']['order_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['order_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['order_id']['link_to_order'] = 1;
/* Champ: Ordered product : Attributs */
$handler->display->display_options['fields']['attributes']['id'] = 'attributes';
$handler->display->display_options['fields']['attributes']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['attributes']['field'] = 'attributes';
$handler->display->display_options['fields']['attributes']['relationship'] = 'products';
$handler->display->display_options['fields']['attributes']['label'] = '';
$handler->display->display_options['fields']['attributes']['exclude'] = TRUE;
$handler->display->display_options['fields']['attributes']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['attributes']['alter']['text'] = '<li>[attributes]</li>';
$handler->display->display_options['fields']['attributes']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['attributes']['list']['separator'] = '</li><li>';
$handler->display->display_options['fields']['attributes']['link_to_entity'] = 0;
/* Champ: Ordered product : Titre */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'products';
$handler->display->display_options['fields']['title']['label'] = 'Produit';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '<p>[title]</p><ul>[attributes]</ul>';
/* Champ: Ordered product : Order Product ID */
$handler->display->display_options['fields']['order_product_id_1']['id'] = 'order_product_id_1';
$handler->display->display_options['fields']['order_product_id_1']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['order_product_id_1']['field'] = 'order_product_id';
$handler->display->display_options['fields']['order_product_id_1']['relationship'] = 'products';
$handler->display->display_options['fields']['order_product_id_1']['label'] = '';
$handler->display->display_options['fields']['order_product_id_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['order_product_id_1']['element_label_colon'] = FALSE;
/* Champ: Contenu : Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['relationship'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Champ: Ordered product : Quantité */
$handler->display->display_options['fields']['qty']['id'] = 'qty';
$handler->display->display_options['fields']['qty']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['qty']['field'] = 'qty';
$handler->display->display_options['fields']['qty']['relationship'] = 'products';
/* Champ: Contenu : Ordered quiz link */
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['id'] = 'uc_quiz_field_ordered_quiz_custom_link';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['table'] = 'node';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['field'] = 'uc_quiz_field_ordered_quiz_custom_link';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['relationship'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['label'] = 'Accès direct';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Filtre contextuel: Utilisateur : Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'users';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['uid']['exception']['value'] = '';
$handler->display->display_options['arguments']['uid']['exception']['title'] = 'Tout';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'user_or_permission';
$handler->display->display_options['arguments']['uid']['validate_options']['perm'] = 'view own orders';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Critère de filtrage: Ordre : Statut de la commande */
$handler->display->display_options['filters']['order_status']['id'] = 'order_status';
$handler->display->display_options['filters']['order_status']['table'] = 'uc_orders';
$handler->display->display_options['filters']['order_status']['field'] = 'order_status';
$handler->display->display_options['filters']['order_status']['value'] = array(
  'completed' => 'completed',
);
$handler->display->display_options['path'] = 'user/%/ordered-quizzes';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Tests de connaissances';
$handler->display->display_options['menu']['description'] = 'Voir mes tests de connaissances';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'user-menu';
$handler->display->display_options['menu']['context'] = 1;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'user_ordered_quizzes_page_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Mes tests de connaissances';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'order_id',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'order_id' => 'order_id',
  'attributes' => 'attributes',
  'title' => 'title',
  'qty' => 'qty',
  'order_product_id_1' => 'order_product_id_1',
  'nid' => 'nid',
  'uc_quiz_field_ordered_quiz_custom_link' => 'uc_quiz_field_ordered_quiz_custom_link',
);
$handler->display->display_options['style_options']['default'] = 'order_id';
$handler->display->display_options['style_options']['info'] = array(
  'order_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => ' ',
    'empty_column' => 0,
  ),
  'attributes' => array(
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'qty' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'order_product_id_1' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'uc_quiz_field_ordered_quiz_custom_link' => array(
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['empty'] = FALSE;
/* Comportement en l'absence de résultats: Global : Zone de texte */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'Pas de commande';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = "Vous n'avez pas encore de commande <strong>validée</strong> de tests de connaissances.";
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
$handler->display->display_options['empty']['area']['tokenize'] = TRUE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relation: Ordre : Produits */
$handler->display->display_options['relationships']['products']['id'] = 'products';
$handler->display->display_options['relationships']['products']['table'] = 'uc_orders';
$handler->display->display_options['relationships']['products']['field'] = 'products';
$handler->display->display_options['relationships']['products']['label'] = 'Produits commandés';
$handler->display->display_options['relationships']['products']['required'] = TRUE;
/* Relation: Ordered product : Nœud */
$handler->display->display_options['relationships']['nid']['id'] = 'nid';
$handler->display->display_options['relationships']['nid']['table'] = 'uc_order_products';
$handler->display->display_options['relationships']['nid']['field'] = 'nid';
$handler->display->display_options['relationships']['nid']['relationship'] = 'products';
$handler->display->display_options['relationships']['nid']['label'] = 'Produit';
$handler->display->display_options['relationships']['nid']['required'] = TRUE;
/* Relation: Entity Reference : Entité référencée */
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['id'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['table'] = 'field_data_field_uc_product_quiz';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['field'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['relationship'] = 'nid';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['label'] = 'Quiz associé au produit';
$handler->display->display_options['relationships']['field_uc_product_quiz_target_id']['required'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Champ: Ordre : Identifiant de la commande */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'uc_orders';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Commande n°';
$handler->display->display_options['fields']['order_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['order_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['order_id']['link_to_order'] = 1;
/* Champ: Ordered product : Attributs */
$handler->display->display_options['fields']['attributes']['id'] = 'attributes';
$handler->display->display_options['fields']['attributes']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['attributes']['field'] = 'attributes';
$handler->display->display_options['fields']['attributes']['relationship'] = 'products';
$handler->display->display_options['fields']['attributes']['label'] = '';
$handler->display->display_options['fields']['attributes']['exclude'] = TRUE;
$handler->display->display_options['fields']['attributes']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['attributes']['alter']['text'] = '<li>[attributes]</li>';
$handler->display->display_options['fields']['attributes']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['attributes']['list']['separator'] = '</li><li>';
$handler->display->display_options['fields']['attributes']['link_to_entity'] = 0;
/* Champ: Ordered product : Titre */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'products';
$handler->display->display_options['fields']['title']['label'] = 'Produit';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '<p>[title]</p><ul>[attributes]</ul>';
/* Champ: Ordered product : Quantité */
$handler->display->display_options['fields']['qty']['id'] = 'qty';
$handler->display->display_options['fields']['qty']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['qty']['field'] = 'qty';
$handler->display->display_options['fields']['qty']['relationship'] = 'products';
/* Champ: Ordered product : Order Product ID */
$handler->display->display_options['fields']['order_product_id_1']['id'] = 'order_product_id_1';
$handler->display->display_options['fields']['order_product_id_1']['table'] = 'uc_order_products';
$handler->display->display_options['fields']['order_product_id_1']['field'] = 'order_product_id';
$handler->display->display_options['fields']['order_product_id_1']['relationship'] = 'products';
$handler->display->display_options['fields']['order_product_id_1']['label'] = '';
$handler->display->display_options['fields']['order_product_id_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['order_product_id_1']['element_label_colon'] = FALSE;
/* Champ: Contenu : Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['relationship'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Champ: Contenu : Ordered quiz link */
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['id'] = 'uc_quiz_field_ordered_quiz_custom_link';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['table'] = 'node';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['field'] = 'uc_quiz_field_ordered_quiz_custom_link';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['relationship'] = 'field_uc_product_quiz_target_id';
$handler->display->display_options['fields']['uc_quiz_field_ordered_quiz_custom_link']['label'] = 'Accès direct';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Filtre contextuel: Utilisateur : Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'users';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['exception']['value'] = '';
$handler->display->display_options['arguments']['uid']['exception']['title'] = 'Tout';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'user_or_permission';
$handler->display->display_options['arguments']['uid']['validate_options']['perm'] = 'view own orders';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Critère de filtrage: Ordre : Statut de la commande */
$handler->display->display_options['filters']['order_status']['id'] = 'order_status';
$handler->display->display_options['filters']['order_status']['table'] = 'uc_orders';
$handler->display->display_options['filters']['order_status']['field'] = 'order_status';
$handler->display->display_options['filters']['order_status']['value'] = array(
  'completed' => 'completed',
);
$handler->display->display_options['path'] = 'mes-tests-de-connaissances';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Tests de connaissances';
$handler->display->display_options['menu']['description'] = 'Voir mes tests de connaissances';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'user-menu';
$handler->display->display_options['menu']['context'] = 1;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['uc_quiz_user_ordered_quizzes'] = array(
  t('Master'),
  t('Mon historique de commandes'),
  t('plus'),
  t('Appliquer'),
  t('Réinitialiser'),
  t('Trier par'),
  t('Asc'),
  t('Desc'),
  t('Éléments par page'),
  t('- Tout -'),
  t('Décalage'),
  t('« premier'),
  t('‹ précédent'),
  t('suivant ›'),
  t('dernier »'),
  t('Pas de commande'),
  t("Vous n'avez pas encore commandé de tests de connaissances."),
  t('produits'),
  t('Produits commandés'),
  t('Quiz associé'),
  t('Date'),
  t('Numéro de commande'),
  t('Etat'),
  t('Montant Total'),
  t('.'),
  t(','),
  t('Actions'),
  t('Tout'),
  t('Page'),
  t('Mes tests de connaissances'),
  t("Vous n'avez pas encore de commande <strong>validée</strong> de tests de connaissances."),
  t('Produit'),
  t('Quiz associé au produit'),
  t('Commande n°'),
  t('<li>[attributes]</li>'),
  t('<p>[title]</p><ul>[attributes]</ul>'),
  t('Quantité'),
  t('Accès direct'),
);
