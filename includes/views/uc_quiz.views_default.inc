<?php
/**
 * @file
 * Handles default views.
 *
 * Reads views from all the .view.inc files in the views/defaults directory.
 */

/**
 * Implements hook_views_default_views().
 */
function uc_quiz_views_default_views() {
  $views = array();

  $path = drupal_get_path('module', 'uc_quiz') . '/includes/views/defaults';
  $view_files = file_scan_directory($path, '/.*\.view\.inc/');
  foreach ($view_files as $file) {
    $view = NULL;
    include DRUPAL_ROOT . '/' . $path . '/' . $file->filename;
    if (!empty($view)) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}
