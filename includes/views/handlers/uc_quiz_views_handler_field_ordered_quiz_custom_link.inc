<?php
/**
 * @file
 * Defines a views custom field handler.
 *
 * A filter which hides the link to an ordered quiz if an
 * another one of the same type is currently in the taking.
 */

/**
 * Views field handler class for ordered quiz link custom field.
 */
class UcQuizViewsHandlerFieldOrderedQuizCustomLink extends views_handler_field_custom {

  /**
   * Overrides views_handler_field_custom::render().
   */
  public function render($values) {
    $render = parent::render($values);
    $render = theme('html_tag', array(
      'element' => array(
        '#tag' => 'p',
        '#attributes' => array(
          'class' => 'text text-warning',
        ),
        '#value' => t('You have to buy this quiz before taking it.'),
      ),
    ));
    if (!empty($values->uc_order_products_uc_orders_title)) {
      $quiz_title = $values->uc_order_products_uc_orders_title;
      $render = theme('html_tag', array(
        'element' => array(
          '#tag' => 'p',
          '#attributes' => array(
            'class' => 'text text-warning',
          ),
          '#value' => t('You have to buy @quiz before taking it.', array(
            '@quiz' => $quiz_title,
          )),
        ),
      ));
      if (!empty($values->node_field_data_field_uc_product_quiz_nid) && !empty($values->uc_orders_order_id) && !empty($values->uc_order_products_uc_orders_order_product_id)) {

        $quiz_nid = $values->node_field_data_field_uc_product_quiz_nid;
        $order_id = $values->uc_orders_order_id;
        $order_product_id = $values->uc_order_products_uc_orders_order_product_id;

        $path = 'node/' . $quiz_nid . '/' . $order_id . '/' . $order_product_id;

        $quiz = uc_quiz_menu_load($quiz_nid, $quiz_nid, $order_id, $order_product_id, FALSE);

        if ($quiz && $uc_quiz_available_qty = uc_quiz_get_uc_quiz_qty($quiz, FALSE)) {
          $quiz_takes_qty = uc_quiz_get_ordered_quiz_result_count(array(
            'quiz_nid' => $quiz_nid,
            'order_id' => $order_id,
            'order_product_id' => $order_product_id,
          ));
          if ($uc_quiz_available_qty > $quiz_takes_qty) {
            $render = theme('html_tag', array(
              'element' => array(
                '#tag' => 'p',
                '#attributes' => array(
                  'class' => 'text text-primary',
                ),
                '#value' => t('You can take this quiz now.'),
              ),
            ));
            $render .= l($quiz_title, $path, array(
              'attributes' => array(
                'class' => 'btn btn-primary',
              ),
            ));

            if (uc_quiz_is_quiz_take_in_progress($quiz_nid)) {
              if (!empty($_SESSION['uc_quiz']) && !empty($_SESSION['uc_quiz'][$quiz_nid])) {
                if (!empty($_SESSION['uc_quiz'][$quiz_nid]['uc_order_id']) && !empty($_SESSION['uc_quiz'][$quiz_nid]['uc_order_product_id'])) {
                  if ($_SESSION['uc_quiz'][$quiz_nid]['uc_order_id'] != $order_id || $_SESSION['uc_quiz'][$quiz_nid]['uc_order_product_id'] != $order_product_id) {
                    $render = theme('html_tag', array(
                      'element' => array(
                        '#tag' => 'p',
                        '#attributes' => array(
                          'class' => 'text text-danger',
                        ),
                        '#value' => t('You have already started a quiz of this type. You have to finish it first before taking this one.'),
                      ),
                    ));
                    $render .= l($quiz_title, $path, array(
                      'attributes' => array(
                        'class' => 'btn btn-danger',
                      ),
                    ));
                  }
                  else {
                    if ($_SESSION['uc_quiz'][$quiz_nid]['uc_order_id'] == $order_id && $_SESSION['uc_quiz'][$quiz_nid]['uc_order_product_id'] == $order_product_id) {
                      $render = theme('html_tag', array(
                        'element' => array(
                          '#tag' => 'p',
                          '#attributes' => array(
                            'class' => 'text text-warning',
                          ),
                          '#value' => t('You have to finish this quiz before taking another one of the same type. Click below to finish it.'),
                        ),
                      ));
                      $render .= l($quiz_title, $path, array(
                        'attributes' => array(
                          'class' => 'btn btn-warning',
                        ),
                      ));
                    }
                  }
                }
              }
            }
          }
          else {
            $render = theme('html_tag', array(
              'element' => array(
                '#tag' => 'p',
                '#attributes' => array(
                  'class' => 'text text-success',
                ),
                '#value' => t('You have finished all the quizzes for this order. Congratulations !'),
              ),
            ));
            $render .= l($quiz_title, $path, array(
              'attributes' => array(
                'class' => 'btn btn-success',
              ),
            ));
          }

          $render .= theme('html_tag', array(
            'element' => array(
              '#tag' => 'p',
              '#attributes' => array(
                'class' => 'text text-muted',
              ),
              '#value' => t('You can still access your quiz results by clicking above.'),
            ),
          ));
        }
      }
    }
    return $render;
  }

}
