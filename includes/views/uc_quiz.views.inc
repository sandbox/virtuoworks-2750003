<?php
/**
 * @file
 * Views hooks.
 *
 * This file provides metadata to the Views 3 API for views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function uc_quiz_views_data_alter(&$data) {
  if (function_exists('quiz_views_data_alter')) {
    quiz_views_data_alter($data);
  }
  else {
    watchdog('uc_quiz', 'Cannot find quiz_views_data_alter() function. Check quiz module.', array(), WATCHDOG_ERROR);
    drupal_set_message(t('Cannot find quiz_views_data_alter() function. Check quiz module.'), 'error', FALSE);
  }

  $data['uc_order_products']['order_product_id'] = array(
    'title' => t('Order Product ID'),
    'help' => t('The Order Product ID. An Order Product represents the relationship between a Product and an Order'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['node']['uc_quiz_field_ordered_quiz_custom_link'] = array(
    'title' => t('Ordered quiz link'),
    'help' => t('The Ordered quiz link.'),
    'label' => t('Direct access to quiz'),
    'relationship' => 'field_uc_product_quiz_target_id',
    'field' => array(
      'handler' => 'UcQuizViewsHandlerFieldOrderedQuizCustomLink',
    ),
  );
}

/**
 * Implements hook_views_pre_view().
 *
 * Replace the static field with dynamic fields.
 */
function uc_quiz_views_pre_view(&$view, &$display_id, &$args) {
  // Nothing here.
}
