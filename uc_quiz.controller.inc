<?php
/**
 * @file
 * Controller classes hooks.
 *
 * Contains controller classes for uc_quiz_ordered_quizzes
 * and uc_quiz_ordered_quiz_results entities.
 */

/**
 * Controller class for uc_quiz_ordered_quiz entity.
 */
class UcQuizOrderedQuizController extends EntityAPIController {

  /**
   * Overrides EntityAPIController::load().
   */
  public function load($ids = array(), $conditions = array()) {
    $ordered_quizzes = parent::load($ids, $conditions);
    foreach ($ordered_quizzes as $ordered_quiz) {
      if (!empty($ordered_quiz->quiz_nid)) {
        if (function_exists('quiz_menu_load')) {
          $ordered_quiz->quiz = quiz_menu_load($ordered_quiz->quiz_nid);
        }
        else {
          watchdog('uc_quiz', 'Cannot find quiz_menu_load() function. Check quiz module.', array(), WATCHDOG_ERROR);
          drupal_set_message(t('Cannot find quiz_menu_load() function. Check quiz module.'), 'error', FALSE);
        }
      }
      if (!empty($ordered_quiz->product_nid)) {
        $ordered_quiz->product = node_load($ordered_quiz->product_nid);
      }
      if (!empty($ordered_quiz->order_id)) {
        if (function_exists('uc_order_load')) {
          $ordered_quiz->uc_order = uc_order_load($ordered_quiz->order_id);
        }
        else {
          watchdog('uc_quiz', 'Cannot find uc_order_load() function. Check ubercart order (uc_order) module.', array(), WATCHDOG_ERROR);
          drupal_set_message(t('Cannot find uc_order_load() function. Check ubercart order (uc_order) module.'), 'error', FALSE);
        }
      }
      if (!empty($ordered_quiz->order_product_id)) {
        if (function_exists('uc_order_product_load')) {
          $ordered_quiz->uc_order_product = uc_order_product_load($ordered_quiz->order_product_id);
        }
        else {
          watchdog('uc_quiz', 'Cannot find uc_order_product_load() function. Check ubercart order (uc_order) module.', array(), WATCHDOG_ERROR);
          drupal_set_message(t('Cannot find uc_order_product_load() function. Check ubercart order (uc_order) module.'), 'error', FALSE);
        }
      }
    }
    return $ordered_quizzes;
  }

}

/**
 * Controller class for uc_quiz_ordered_quiz_result entity.
 */
class UcQuizOrderedQuizResultController extends EntityAPIController {

  /**
   * Overrides EntityAPIController::load().
   */
  public function load($ids = array(), $conditions = array()) {
    $ordered_quizzes_results = parent::load($ids, $conditions);
    foreach ($ordered_quizzes_results as $ordered_quiz_result) {
      if (!empty($ordered_quiz_result->ordered_quiz_id)) {
        $ordered_quiz_result->ordered_quiz = entity_load_single('uc_quiz_ordered_quiz', $ordered_quiz_result->ordered_quiz_id);
      }
      if (!empty($ordered_quiz_result->result_id)) {
        $ordered_quiz_result->result = quiz_result_load($ordered_quiz_result->result_id);
      }
    }
    return $ordered_quizzes_results;
  }

}

/**
 * Overrides Controller class for quiz entity.
 */
class UcQuizQuizController extends QuizController {
  // Nothing here.
}

/**
 * Overrides Controller class for the uc_order_product entity.
 */
class UcQuizOrderProductController extends UcOrderProductController {

  /**
   * Overrides UcOrderProductController::buildContent().
   */
  public function buildContent($product, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content = parent::buildContent($product, $view_mode, $langcode, $content);

    $ordered_quiz = uc_quiz_uc_order_product_load($product);

    if (!empty($ordered_quiz) && !empty($ordered_quiz->quiz_nid)) {
      $order = uc_order_load($ordered_quiz->order_id);
      if (!empty($order)) {
        $quiz = node_load($ordered_quiz->quiz_nid);
        if (!empty($quiz)) {
          if (!empty($order->order_status) && in_array($order->order_status, array('payment_received', 'completed'))) {
            $title = node_access('view', $quiz) ? l($quiz->title, 'node/' . $quiz->nid . '/' . $ordered_quiz->order_id . '/' . $ordered_quiz->order_product_id) : check_plain($quiz->title);
          }
          else {
            $title = check_plain($quiz->title);
          }

          $content['product'] = array(
            '#markup' => $title . uc_product_get_description($product),
            '#cell_attributes' => array('class' => array('product')),
          );
        }
      }
    }

    return $content;
  }

}

/**
 * EntityOrderedQuiz Views Controller class.
 */
class UcQuizEntityOrderedQuizViewsController extends EntityDefaultViewsController {
  // Nothing here.
}

/**
 * EntityOrderedQuizResult Views Controller class.
 */
class UcQuizEntityOrderedQuizResultViewsController extends EntityDefaultViewsController {
  // Nothing here.
}
