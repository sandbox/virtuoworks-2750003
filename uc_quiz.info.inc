<?php
/**
 * @file
 * Entity Metadata hooks.
 */

/**
 * Metadata controller class for uc_quiz_ordered_quiz entities.
 */
class UcQuizOrderedQuizMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides EntityDefaultMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $properties = parent::entityPropertyInfo();
    // Copy the descriptions from the schema. Drupal discards this
    // information, so we have to call uc_quiz_schema() directly.
    module_load_include('install', 'uc_quiz', 'uc_quiz');
    $schema = uc_quiz_schema();
    foreach ($schema['uc_quiz_ordered_quizzes']['fields'] as $name => $info) {
      if (is_array($properties['uc_quiz_ordered_quiz']['properties'][$name]) && !empty($info['description'])) {
        $properties['uc_quiz_ordered_quiz']['properties'][$name]['description'] = $info['description'];
      }
    }

    return $properties;
  }

}

/**
 * Metadata controller class for uc_quiz_ordered_quiz_result entities.
 */
class UcQuizOrderedQuizResultMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides EntityDefaultMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $properties = parent::entityPropertyInfo();
    // Copy the descriptions from the schema. Drupal discards this
    // information, so we have to call uc_quiz_schema() directly.
    module_load_include('install', 'uc_quiz', 'uc_quiz');
    $schema = uc_quiz_schema();
    foreach ($schema['uc_quiz_ordered_quiz_results']['fields'] as $name => $info) {
      if (is_array($properties['uc_quiz_ordered_quiz_result']['properties'][$name]) && !empty($info['description'])) {
        $properties['uc_quiz_ordered_quiz_result']['properties'][$name]['description'] = $info['description'];
      }
    }
    return $properties;
  }

}

/**
 * Overrides Metadata controller class for uc_order_product entities.
 */
class UcQuizOrderProductMetadataController extends UcOrderProductMetadataController {

  /**
   * Overrides UcOrderProductMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $properties = parent::entityPropertyInfo();

    // Add the 'attributes' property.
    $properties['uc_order_product']['properties']['attributes'] = array(
      'type' => 'list<text>',
      'label' => t('Attributes'),
      'description' => t('The product attributes.'),
      'getter callback' => 'uc_quiz_product_attributes_property_get',
    );

    return $properties;
  }

}
